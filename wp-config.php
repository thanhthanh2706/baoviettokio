<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'wordpress' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't E.(t(PDWb(52k`/c>d6O-uDF$mROrYD&+nuN~X{a<3hHaTU2`,d_5q{VT_:Y_}' );
define( 'SECURE_AUTH_KEY',  '3O)$z1; #6A`/3j6.w*q-Sx97T89c }t99KF=SrATCn7LvEhmtU0@3QOEJd:0H>{' );
define( 'LOGGED_IN_KEY',    'UnO[+-81X{AP%[VvR`QqybKFNCCxv4Umpffve~gbyGloJ&],ERwmgGcmDm6zTpIX' );
define( 'NONCE_KEY',        'vtSk-!x~*bg?s%g!GE+FFX0E<6pe~+F5x663k[ye)+/h0u ;X5yC3:kAyi{O(]0*' );
define( 'AUTH_SALT',        'g@dK?c{FsJvdv/@yuN{hN*DA{!1KUekmze}l2Kj`rcD;)-HG?tler*l{PJ#H+>^e' );
define( 'SECURE_AUTH_SALT', 'aR16{|Y)?;lk]N0@@Jvp*ssp `A|PF)yH{:Qcitgj~d1}h<KPFZ>Dm@-R0elS)*s' );
define( 'LOGGED_IN_SALT',   '}Xf[Jpc4ZRc^;y?i}I0Y8u&Up,1f]Z$_@kX(G={?M)2-:;t+W|?,v|5f:.([>;gn' );
define( 'NONCE_SALT',       '+M]#|rmaOL8p;5WIQswHiZLxVx?]Cbg1 LVeJ%4%LhDpo?4r&yfkA!9.1EnjT,m;' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
