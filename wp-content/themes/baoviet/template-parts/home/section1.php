<div class="h-section1">
    <div class="tw_container">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="--text" data-aos="zoom-in" data-aos-delay="300">
                        <div class="line">
                            <hr>
                            <p> BAOVIET TOKIO MARINE</p>
                        </div>
                        <h1>CHƯƠNG TRÌNH </h1>
                        <h1>TRI ÂN KHÁCH HÀNG THÁNG 4</h1>
                        <div class="_txt">
                            <P>CHƯƠNG TRÌNH TRI ÂN <br> KHÁCH HÀNG THÁNG 4</p> 
                        </div>
                        <a href="javascript:void(0)" class="tw_see_all">Xem Thêm</a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="--text">
                        <div class="line">
                            <hr>
                            <p> BAOVIET TOKIO MARINE</p>
                        </div>
                        <h1>CHƯƠNG TRÌNH </h1>
                        <h1>TRI ÂN KHÁCH HÀNG THÁNG 4</h1>
                        <a href="javascript:void(0)" class="tw_see_all">Xem Thêm</a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="--text">
                        <div class="line">
                            <hr>
                            <p> BAOVIET TOKIO MARINE</p>
                        </div>
                        <h1>CHƯƠNG TRÌNH </h1>
                        <h1>TRI ÂN KHÁCH HÀNG THÁNG 4</h1>
                        <a href="javascript:void(0)" class="tw_see_all">Xem Thêm</a>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="--text">
                        <div class="line">
                            <hr>
                            <p> BAOVIET TOKIO MARINE</p>
                        </div>
                        <h1>CHƯƠNG TRÌNH </h1>
                        <h1>TRI ÂN KHÁCH HÀNG THÁNG 4</h1>
                        <a href="javascript:void(0)" class="tw_see_all">Xem Thêm</a>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
        <div class="contact_banner">
            <a href="javascript:void(0)">
                <div class="support">
                    <div class="phone">
                        <img src="<?php echo THEME_ASSETS . '/images/section1/phone.png'?>" alt="">
                    </div>
                    <div class="info">
                        <p>Tư vấn dịch vụ </p>
                        <span class="info1">0915 341 499</span>
                    </div>
                </div>
            </a>
            <a href="javascript:void(0)">
                <div class="support">
                    <div class="user">
                    <img src="<?php echo THEME_ASSETS . '/images/section1/user.png'?>" alt="">
                    </div>
                    <div class="info">
                        <p>Hỗ trợ bồi thường</p>
                        <span>1900 0000</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<script>
    var swiper1 = new Swiper('.h-section1 .swiper-container', {
        pagination: {
            el: '.h-section1 .swiper-pagination',
            clickable: true,
        },
    });
</script>