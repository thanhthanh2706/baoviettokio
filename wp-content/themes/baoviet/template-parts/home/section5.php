<div class="h-section5">
    <div class="tw_container">
        <div class="content_all">
            <div class="--left" data-aos="fade-right">
            </div>
            <div class="--right" data-aos="fade-left">
                <div class="tw_title">
                    <hr>
                    <h2>GIỚI THIỆU <br> BẢO VIỆT TOKIO MARINE</h2>
                    <h3>“TO BE A GOOD COMPANY <br> IN VIET NAM”</h3>
                </div>
                <div class="img_SS5" data-aos="fade-right"></div>
                <div class="tw_main">
                    <p>Được thành lập năm 1996, <span>Bảo Việt Tokio Marine</span>  là liên doanh bảo hiểm phi nhân thọ có vốn đầu tư nước ngoài đầu tiên tại Việt Nam với các đối tác hiện tại là Tập đoàn Tokio Marine - Một trong những tập đoàn bảo hiểm hàng đầu Nhật Bản và Tập đoàn Bảo Việt - Tập đoàn Tài chính Bảo hiểm hàng đầu tại Việt Nam.</p> 
                    <p>Với triết lý kinh doanh bền vững,<span>Bao Viet Tokio Marine</span> luôn phấn đấu trở thành nhà bảo hiểm chất lượng Nhật Bản hàng đầu tại thị trường Việt Nam, mang lại những lợi ích và giá trị ưu việt tới khách hàng, đối tác và xã hội.</p>
                </div>
                <a href="javascript:void(0)" class="tw_see_all" data-aos="fade-up">Xem Thêm</a>
            </div>
        </div>
    </div>
</div>