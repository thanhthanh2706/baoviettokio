<div class="h-section3">
    <div class="tw_container">
        <div class="swiper-button-next swp_btn">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="swiper-button-prev swp_btn">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </div>
        <div class="content_all">
            <div class="tw_title">
                <hr>
                <h2 data-aos="fade-right">Bảo hiểm cá nhân</h2>
                <h3 data-aos="fade-left">Tận hưởng cuộc sống <br> không giới hạn</h3>
            </div>
            <div class="swiper-container" data-aos="zoom-in">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" style="background-image: url('<?php echo THEME_ASSETS . '/images/section3/imgs31.png'; ?>')">
                        <div class="cover_img"></div>
                        <div class="--txt">
                            <img src="<?php echo THEME_ASSETS . '/images/section3/plane.png' ?>" alt="">
                            <p>Bảo hiểm </p>
                            <p> Du lịch toàn cầu Travel Mate </p>
                            <p class="--hidden_1">Bảo vệ cho ngôi nhà và những người thân yêu của bạn</p>
                            <p class="--hidden_2">Xem chi tiết
                                <i class="fas fa-caret-down"></i>
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image: url('<?php echo THEME_ASSETS . '/images/section3/imgs31.png'; ?>')">
                        <div class="cover_img"></div>
                        <div class="--txt">
                            <img src="<?php echo THEME_ASSETS . '/images/section3/house.png' ?>" alt="">
                            <p>Bảo hiểm nhà</p>
                            <p> </p>
                            <p class="--hidden_1">Bảo vệ cho ngôi nhà và những người thân yêu của bạn</p>
                            <a href="#">
                                <p class="--hidden_2">Xem chi tiết
                                    <i class="fas fa-caret-down"></i>
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image: url('<?php echo THEME_ASSETS . '/images/section3/imgs33.png'; ?>')">
                        <div class="cover_img"></div>
                        <div class="--txt">
                            <img src="<?php echo THEME_ASSETS . '/images/section3/golf-ball-with-dents.png' ?>" alt="">
                            <p>Bảo hiểm </p>
                            <p> Dành cho người chơi Golf </p>
                            <p class="--hidden_1">Bảo vệ cho ngôi nhà và những người thân yêu của bạn</p>
                            <a href="#">
                                <p class="--hidden_2">Xem chi tiết
                                    <i class="fas fa-caret-down"></i>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div class="under_ss3">
            <div class="--left" data-aos="fade-right">
                <div id="tab01" class="tab_s3 tabcontent">
                    <div class="tw_title">
                        <hr>
                        <h2>BỒI THƯỜNG</h2>
                        <h3> CHẤT LƯỢNG NHẬT BẢN <br> CAM KẾT VỮNG BỀN</h3>
                    </div>
                    <div class="tw_main">
                        <p>Cam kết làm hài lòng khách hàng bằng sản phẩm và dịch vụ chất lượng tốt nhất. <span>Bảo Việt Tokio Marine</span> hiểu rằng việc giải quyết bồi thường nhanh chóng thoả đáng sẽ giúp cho khách hàng nhanh chóng khôi phục những khó khăn tạm thời do sự cố rủi ro gây ra.</p>
                    </div>
                </div>
                <div id="tab02" class="tabcontent">

                </div>
                <div class="buttonSS3">
                    <div class="btn btn1 tablinks" data-src="#tab01">
                        <p>CHÍNH SÁCH BỒI THƯỜNG</p>
                    </div>
                    <div class="btn btn2 tablinks" data-src="#tab02">
                        <p>HƯỚNG DẪN BỒI THƯỜNG</p>
                    </div>
                </div>
            </div>
            <div class="--right" data-aos="fade-left">
                <img src="<?php echo THEME_ASSETS . '/images/section3/infoglobal.png' ?>" alt="">
            </div>
        </div>
    </div>
</div>
<script>
    var mySwiper = new Swiper('.h-section3 .swiper-container', {
        // autoplay: {
        //     delay: 1900
        // },
        speed: 400,
        loop: true,
        slidesPerView: 3,
        spaceBetween: 30,
        breakpoints: {
            //max-width =480px
            480: {
                slidesPerView: 1.4,
                spaceBetween: 30,
            },
            //max-width =768px
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 2.5,
                spaceBetween: 30,
            },
        },
        pagination: {
            el: '.h-section3 .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.h-section3 .swiper-button-next',
            prevEl: '.h-section3 .swiper-button-prev',
        },
    });
    //btn
    $(".btn1").click(function() {
        $(this).css("background-color", "#0079c0");
        $(this).css("transition", ".4s");
        $(this).css({
            'color': '#ffffff'
        })
        $(".btn2").click(function() {
            $(".btn1").css("background-color", "#ffffff");
            $(".btn1").css("transition", ".4s");
            $(".btn1").css({
                'color': '#0079c0'
            })
        });
    });
    $(".btn2").click(function() {
        $(".btn1").css("background-color", "#ffffff");
        $(".btn1").css("transition", ".4s");
        $(".btn1").css({
            'color': '#0079c0'
        })
    });
    $(".btn2").click(function() {
        $(this).css("background-color", "#0079c0");
        $(this).css("transition", ".4s");
        $(this).css({
            'color': '#ffffff'
        })
        $(".btn1").click(function() {
            $(".btn2").css("background-color", "#ffffff");
            $(".btn2").css({
                'color': '#0079c0'
            })
        });
    });
    //tab
    $('.btn').click(function() {
        activeTab(this);
        return false;
    });
    // active trang dau tien duoc chay 
    activeTab($('.btn1'));
    // active 1 tab bat ki 
    function activeTab(obj) {
        // Xóa class active tất cả các tab
        $('.btn').removeClass('active');
        // Thêm class active vòa tab đang click
        $(obj).addClass('active');
        // Lấy attr của tab để show content tương ứng  
        // var id = $attr('data-head');
        var id = $(obj).attr('data-src');
        // Ẩn hết nội dung các tab đang hiển thị
        $('.tabcontent').hide();
        // Hiển thị nội dung của tab hiện tại
        $(id).show();
    }
</script>