<div class="h-section4">
    <div class="tw_container">
        <div class="swiper-button-next swp_btn">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="swiper-button-prev swp_btn">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </div>
        <div class="content_all">
            <div class="tw_title">
                <hr>
                <h2 data-aos="fade-left">Tin tức</h2>
                <h3 data-aos="fade-right">Góc chia sẻ</h3>
            </div>
            <div class="swiper-container" data-aos="zoom-in">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <a href="javascript:void(0)">
                            <div class="imgs4" style="background-image: url('<?php echo THEME_ASSETS . '/images/section4/imgs41.png'; ?>')">
                                <div class="--txt_img">
                                    <hr>
                                    <p>DU LỊCH</p>
                                </div>
                            </div>
                            <div class="--text">
                                <p>Thứ 2, 11/04/2019</p>
                                <P>Cuộc sống trên đảo: Điểm đến kỳ thú nhất ở Châu Á</P>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="javascript:void(0)">
                            <div class="imgs4" style="background-image: url('<?php echo THEME_ASSETS . '/images/section4/imgs42.png'; ?>')">
                                <div class="--txt_img">
                                    <hr>
                                    <p>XE CỘ</p>
                                </div>
                            </div>
                            <div class="--text">
                                <p>Thứ 2, 11/04/2019</p>
                                <P>Bảo hiểm ô tô và các mẹo được chia sẻ khắp thế giới</P>
                            </div>
                        </a>
                    </div>
                    <div class="swiper-slide">
                        <a href="javascript:void(0)">
                            <div class="imgs4" style="background-image: url('<?php echo THEME_ASSETS . '/images/section4/imgs43.png'; ?>')">
                                <div class="--txt_img">
                                    <hr>
                                    <p>ĐỜI SỐNG</p>
                                </div>
                            </div>
                            <div class="--text">
                                <p>Thứ 2, 11/04/2019</p>
                                <P>Bảo hiểm phi nhân thọ - Sự lựa chọn bảo vệ toàn diện</P>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="_list_share">
                <div class="_item">
                    <div class="_img" style="background-image: url('<?php echo THEME_ASSETS . '/images/section4/imgs41.png'; ?>')">
                        <div class="__title">
                            <hr>
                            <p>Du lịch</p>
                        </div>
                    </div>
                    <div class="_text">
                        <p class="_nomarl">
                            Thứ 2, 11/04/2019
                        </p>
                        <h3 class="_blod">
                            Cuộc sống trên đảo: Điểm đến kỳ thú nhất ở Châu Á
                        </h3>
                        <a href="javascript:void(0)" class="_show_detail">Xem chi tiết <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
                <div class="_item">
                    <div class="_img" style="background-image: url('<?php echo THEME_ASSETS . '/images/section4/imgs42.png'; ?>')">
                        <div class="__title">
                            <hr>
                            <p >Du lịch</p>
                        </div>
                    </div>
                    <div class="_text">
                        <p class="_nomarl">
                            Thứ 2, 11/04/2019
                        </p>
                        <h3 class="_blod">
                            Cuộc sống trên đảo: Điểm đến kỳ thú nhất ở Châu Á
                        </h3>
                        <a href="javascript:void(0)" class="_show_detail">Xem chi tiết <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
                <div class="_item">
                    <div class="_img" style="background-image: url('<?php echo THEME_ASSETS . '/images/section4/imgs42.png'; ?>')">
                        <div class="__title">
                            <hr>
                            <p >Du lịch</p>
                        </div>
                    </div>
                    <div class="_text">
                        <p class="_nomarl">
                            Thứ 2, 11/04/2019
                        </p>
                        <h3 class="_blod">
                            Cuộc sống trên đảo: Điểm đến kỳ thú nhất ở Châu Á
                        </h3>
                        <a href="javascript:void(0)" class="_show_detail">Xem chi tiết <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <a href="javascript:void(0)" class="tw_see_all" data-aos="fade-up">Xem Thêm</a>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>
<script>
    var mySwiper = new Swiper('.h-section4 .swiper-container', {
        autoplay: {
            delay: 1900
        },
        speed: 400,
        loop: true,
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
        768: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
        },
    });
</script>