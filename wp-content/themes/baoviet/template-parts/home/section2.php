<div class="h-section2">
    <div class="tw_container">
        <div class="content_all">
            <div class="tw_title">
                <hr>
                <h2 data-aos="fade-right">BẢO HIỂM DOANH NGHIỆP</h2>
                <h3 data-aos="fade-left">AN TÂM KINH DOANH <span>-</span><br class="line_down"> Không ngừng vươn xa</h3>
            </div>
            <div class="tw_main">
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="100">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/building.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/buildingwhite.png' ?>" alt="">
                        <p>Bảo hiểm tài sản </p>
                        <p>Sản phẩm bảo hiểm ưu việt nhất cho tài sản và lợi nhuận kinh doanh của doanh nghiệp </p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i> </p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="200">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/crane.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/cranewhite.png' ?>" alt="">
                        <p>Bảo hiểm xây dựng lắp đặt </p>
                        <p>Giải pháp bảo hiểm cho các nhà đầu tư, nhà thầu và nhà thầu phụ</p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i> </p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="300">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Frame.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Framewhite.png' ?>" alt="">
                        <p>Bảo hiểm hàng hải</p>
                        <p>Vững bước vươn xa cùng bảo hiểm hàng hóa và thân tàu</p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i> </p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="400">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Group 9.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Group 9white.png' ?>" alt="">
                        <p>Bảo hiểm tín dụng thương mại</p>
                        <p>Đảm bảo an toàn tài chính tối đa cho doanh nghiệp với bảo hiểm cho các khoản phải thu</p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i> </p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="500">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/hamer.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Group 13white.png' ?>" alt="">
                        <p>Bảo hiểm trách nhiệm</p>
                        <p>Khẳng định uy tín doanh nghiệp trước những trách nhiệm pháp lý đối với nhân viên và bên thứ ba </p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i></p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="600">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/icons.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Group 14white.png' ?>" alt="">
                        <p> Bảo hiểm tài chính</p>
                        <p>Giải pháp bảo hiểm đột phá cho những rủi ro trong môi trường kinh doanh hiện đại</p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i></p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="700">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/car-insurance.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Group 16white.png' ?>" alt="">
                        <p>Bảo hiểm xe cơ giới</p>
                        <p>An tâm vững lái trên mọi nẻo đường với sự đồng hành của đối tác bảo hiểm</p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i></p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main" data-aos="zoom-in" data-aos-delay="800">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Page 1.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/Group 17white.png' ?>" alt="">
                        <p>Bảo hiểm sức khoẻ và tai nạn</p>
                        <p>Bảo vệ cho sức khỏe và sự an toàn của đội ngũ nhân viên chính là bảo vệ cho doanh nghiệp</p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i></p>
                    </div>
                </a>
                <a href="javascript:void(0)">
                    <div class="info_main " data-aos="zoom-in" data-aos-delay="900">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/mode-circular-button.png' ?>" alt="">
                        <img src="<?php echo THEME_ASSETS . '/images/section2/morewhite.png' ?>" alt="">
                        <p>Bảo hiểm khác</p>
                        <p>Tấm lá chắn hiệu quả cho hoạt động kinh doanh</p>
                        <p>Xem chi tiết <i class="fas fa-caret-down"></i></p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<script>

</script>