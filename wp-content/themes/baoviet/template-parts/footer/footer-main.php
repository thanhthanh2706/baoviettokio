<div class="footer_main">
    <div class="tw_container">
        <div class="content_all">
            <div class="footer_up">
                <div class="logo">
                    <img src="<?php echo THEME_ASSETS . '/images/footer/tokio.png' ?>" alt="">
                    <div class="logo2">
                        <img src="<?php echo THEME_ASSETS . '/images/footer/tokiounder.png' ?>" alt="">
                        <div class="txt_logo">
                            <p> MẠNG LƯỚI TOÀN CẦU </p>
                            <p>TOKIO MARINE</p>
                        </div>
                    </div>
                </div>
                <div class="ft_center">
                    <div class="ft_centerup">
                        <div class="ft_1">
                            <p>Sản phẩm</p>
                            <p>Bảo hiểm thương mại </p>
                            <p>Bảo hiểm cá nhân </p>
                        </div>
                        <div class="ft_1">
                            <p>Dịch vụ</p>
                            <p>Quản lý rủi ro </p>
                            <p> Dịch vụ chuyên sâu</p>
                        </div>
                    </div>
                    <div class="ft_2">
                        <p>Trụ sở chính</p>
                        <p>P.601, Tầng 6, Tòa nhà Mặt Trời Sông Hồng,</p>
                        <p>23 Phan Chu Trinh, Quận Hoàn Kiếm, Hà Nội</p>
                        <p>Điện thoại : (84.24) 3933 0704 </p>
                    </div>
                </div>
                <div class="ft_right">
                    <div class="ft_rightup">
                        <div class="ft_1">
                            <p>Bồi thường</p>
                            <p>Quy trình bồi thường </p>
                            <p>Hướng dẫn bồi thường </p>
                            <p> Mạng lưới bồi thường </p>
                        </div>
                        <div class="ft_1 ft_right1">
                            <p>Liên hệ</p>
                            <p>Phản hồi chất </p>
                            <p> lượng dịch vụ</p>
                        </div>
                    </div>
                    <div class="ft_2">
                        <p> Chi nhánh</p>
                        <p>Phòng 3, Lầu 19, Tòa nhà Green Power, 35</p>
                        <p>Tôn Đức Thắng, Quận 1, Tp. Hồ Chí Minh</p>
                        <p>Điện thoại: (84.28) 3822 1340 </p>
                    </div>
                </div>
            </div>
            <div class="footer_down">
                <p>Copyright @ Bao Viet Tokio Marine Insurance Company Limited.</p>
                <p>
                    Theo dõi BVTM trên:
                    <a href="javascript:void(0)"><img src="<?php echo THEME_ASSETS . '/images/footer/f.png' ?>" alt=""></a>
                </p>
            </div>
        </div>
    </div>
</div>