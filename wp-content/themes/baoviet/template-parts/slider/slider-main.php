<!-- slider -->
<div class="slide_main">
    <div class="_item open_menu">
        <div class="_img">
            <img src="<?php echo THEME_ASSETS . '/images/slide_main/slide_main1.png' ?>" alt="">
        </div>
        <p>Menu</p>
    </div>
    <div class="_item">
        <a href="">
            <div class="_img">
                <img src="<?php echo THEME_ASSETS . '/images/slide_main/slide_main2.png' ?>" alt="">
            </div>
            <p>Tư vấn dịch vụ</p>
        </a>
    </div>
    <div class="_item">
        <a href="">
            <div class="_img">
                <img src="<?php echo THEME_ASSETS . '/images/slide_main/slide_main3.png' ?>" alt="">
            </div>
            <p>Hỗ trợ bồi thường</p>
        </a>
    </div>
</div>
<div class="close_menu_mobile"></div>
<div class="menu_mb">
    <ul>
        <li>VI
        <i class="fas fa-caret-down"></i>
        </li>
        <li>
            <div class="_form_search js_form_search">
                <form action="javascript:void(0)" method="POST" id="frm_search" class="frm_search" name="frm_search">
                    <input type="text" id="txt_search" class="txt_search" name="key" placeholder="Tìm kiếm ...">
                    <button class="btn_search" id="btn_search" name="btn_search" type="submit">
                        <img src="<?php echo THEME_ASSETS . '/images/menu-header/search.png'; ?>" alt="">
                    </button>
                </form>
            </div>
        </li>
        <li><a href="javascript:void(0)">
                Bảo hiểm doanh nghiệp
                <i class="fas fa-caret-down"></i>
            </a>
            <ul>
                <li><a href="javascript:void(0)">Bảo hiểm A </a></li>
                <li><a href="javascript:void(0)">Bảo hiểm B </a></li>
                <li><a href="javascript:void(0)">Bảo hiểm C</a></li>
            </ul>
        </li>
        <li><a href="javascript:void(0)">
                Bảo hiểm cá nhân
                <i class="fas fa-caret-down"></i>
            </a>
        </li>
        <li><a href="javascript:void(0)">
                Dịch vụ
                <i class="fas fa-caret-down"></i>
            </a>
        </li>
        <li><a href="javascript:void(0)">
                Bồi thường
                <i class="fas fa-caret-down"></i>
            </a>
        </li>
    </ul>
    <div class="close_menu">
        <img src="<?php echo THEME_ASSETS . '/images/slide_main/slide_main.png' ?>" alt="">
    </div>
</div>

<script>

 // click menu mobile
 jQuery(document).ready(function($) {
        var screen_width = $(window).width();
        if (screen_width < 769) {
            $('.menu_mb>ul>li').click(function() {
                $(this).children('ul').toggleClass('active');
            });
        }
    });
// animation
    jQuery(document).ready(function($) {
        //js AOS:
        AOS.init({
            duration: 700,
            easing: 'linear',
            once: true,
        });
    });

    //swp_btn
    $(".swiper-button-next").click(function() {
        $(this).css("background-color", "#0079c0");
        $(this).css("transition", ".4s");
        $(this).children('i').css({
            'color': '#ffffff'
        })
        $(".swiper-button-prev").click(function() {
            $(".swiper-button-next").css("background-color", "#ffffff");
            $(".swiper-button-next").css("transition", ".4s");
            $(".swiper-button-next").children('i').css({
                'color': '#0079c0'
            })
        });
    });
    $(".swiper-button-prev").click(function() {
        $(".swiper-button-next").css("background-color", "#ffffff");
        $(".swiper-button-next").css("transition", ".4s");
        $(".swiper-button-next").children('i').css({
            'color': '#0079c0'
        })
    });
    $(".swiper-button-prev").click(function() {
        $(this).css("background-color", "#0079c0");
        $(this).css("transition", ".4s");
        $(this).children('i').css({
            'color': '#ffffff'
        })
        $(".swiper-button-next").click(function() {
            $(".swiper-button-prev").css("background-color", "#ffffff");
            $(".swiper-button-next").css("transition", ".4s");
            $(".swiper-button-prev").children('i').css({
                'color': '#0079c0'
            })
        });
    });
//close menu mobile
    $('.open_menu').click(function() {
        $('.menu_mb').css({'transform':'translate(0)'});
        $('.menu_mb').css({'transition':'.4s'});
        $('.close_menu_mobile').css({'display':'block'});
    });
    $('.close_menu').click(function() {
        $('.menu_mb').css({'transform':'translate(-100%)'});
        $('.menu_mb').css({'transition':'.4s'});
        $('.close_menu_mobile').css({'display':'none'});
    });
    $('.close_menu_mobile').click(function() {
        $('.menu_mb').css({'transform':'translate(-100%)'});
        $('.menu_mb').css({'transition':'.4s'});
        $('.close_menu_mobile').css({'display':'none'});
    });
    
</script>