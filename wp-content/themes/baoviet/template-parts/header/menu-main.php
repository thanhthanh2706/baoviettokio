<div class="header_main">
    <div class="tw_container" data-aos="fade-down">
        <div class="content_all">
            <div class="menu_up menu">
                <ul>
                    <li>
                        <a href="javascript:void(0)">Giới thiệu
                            <i class="fas fa-caret-down"></i></a>

                    </li>
                    <li>
                        <a href="javascript:void(0)">Tin tức</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Cơ hội nghề nghiệp
                            <i class="fas fa-caret-down"></i></a>
                        <ul class="drop_menu">
                            <li><a href="">Nghe nghiep 1 </a></li>
                            <li><a href=""> Nghe nghiep 2</a></li>
                            <li><a href=""> Nghe nghiep 3</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Liên hệ </a>
                    </li>
                    <li>
                        <a> VI </a>
                        <i class="fas fa-caret-down"></i>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content_main">
            <div class="menu_down menu">
                <div class="logo">
                    <a href="javascript:void(0)">
                        <img src="<?php echo THEME_ASSETS . '/images/menu-header/logo.png'; ?>" alt="">
                    </a>
                </div>
                <ul>
                    <li>
                        <a href="javascript:void(0)">Bảo hiểm doanh nghiệp
                            <i class="fas fa-caret-down"></i> </a>
                        <ul class="drop_menu">
                            <li><a href="">Nghe nghiep 1 </a></li>
                            <li><a href=""> Nghe nghiep 2</a></li>
                            <li><a href=""> Nghe nghiep 3</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Bảo hiểm cá nhân
                            <i class="fas fa-caret-down"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0) ">Dịch vụ
                            <i class="fas fa-caret-down"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Bồi thường
                            <i class="fas fa-caret-down"></i></a>
                    </li>
                    <li>
                        <div class="search"><img src="<?php echo THEME_ASSETS . '/images/menu-header/search.png'; ?>" alt=""></div>
                        <div class="_form_search js_form_search">
                            <form action="javascript:void(0)" method="POST" id="frm_search" class="frm_search" name="frm_search">
                                <input type="text" id="txt_search" class="txt_search" name="key" placeholder="Tìm kiếm ...">
                                <button class="btn_search" id="btn_search" name="btn_search" type="submit">
                                    <img src="<?php echo THEME_ASSETS . '/images/menu-header/search.png'; ?>" alt="">
                                </button>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.menu>ul>li').hover(function() {
        $(this).children('ul').slideDown();
    }, function() {
        $(this).children('ul').slideUp();
    });
    jQuery(document).ready(function($) {
        var screen_width = $(window).width();
        // FIXME: show/hide search
        if (screen_width > 768) {
            $('.search').click(function() {
                $('.js_form_search').toggleClass('active');
            });
        }
    });
    //search -chua xu ly 
    // $('.search').click(function() {
    //     $('._form_search').css({
    //         'transition': '.3s',
    //         'height': '60px',
    //     });
    //     $('.frm_search').css({
    //         'display': 'flex',
    //     });
    // });
    // $("js_form_search").click(function() {
    //         $('._form_search').css({
    //             'transition': '.3s',
    //             'height': '0',
    //         });
    //         $('.frm_search').css({
    //             'display': 'none',
    //         });
    //     });
</script>