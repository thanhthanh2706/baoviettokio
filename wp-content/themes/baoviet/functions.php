<?php

define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('THEME_ASSETS', get_template_directory_uri() . '/assets');
define('ADMIN_AJAX_URL', admin_url('admin-ajax.php'));
define('HOME_URL', home_url('/'));

//options page
require_once('inc/Options.php');

//contact form
require_once('inc/contact-form/ContactForm.php');
$contactFrom = new ContactForm(array(
    'name', 'email', 'phone', 'address', 'message', 'submit_nonce'
));

class MainCore {
    private static $_instance = null;

    function __construct() {
        add_action('after_setup_theme', array($this, 'afterSetupTheme'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));
    }

    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function afterSetupTheme () {
        load_theme_textdomain('corex', get_template_directory() . '/languages');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
    }

    public function enqueue() {
        wp_enqueue_style('styles', THEME_URI . '/style.css');

        wp_enqueue_style('swiper-css', 'https://unpkg.com/swiper/css/swiper.css');

        wp_enqueue_style('swiper-min-css', 'https://unpkg.com/swiper/css/swiper.min.css');

        wp_enqueue_style('font-awesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

        wp_enqueue_style('font-ansomeware-css', 'https://kit-free.fontawesome.com/releases/latest/css/free.min.css');
      
        wp_enqueue_script('scripts', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array('jquery'));

        wp_enqueue_style('fancybox-css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css');

        wp_enqueue_style('aos-css', 'https://unpkg.com/aos@2.3.1/dist/aos.css');

        wp_enqueue_script('scripts', THEME_URI . '/assets/js/scripts.js', array('jquery'));

        wp_enqueue_script('swiper-js', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js', array('jquery'));

        wp_enqueue_script('fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array('jquery'));

        wp_enqueue_script('aos-js', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array('jquery'));

        $wp_script_data = array(
            'AJAX_URL' => ADMIN_AJAX_URL,
            'HOME_URL' => HOME_URL
        );

        wp_localize_script('scripts', 'obj', $wp_script_data);
    }
}

MainCore::instance();
