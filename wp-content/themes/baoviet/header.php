<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11"/>
    <link type="image/x-icon" rel="shortcut icon" href="<?php echo THEME_ASSETS . '/images/favicon.png'; ?>">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap&subset=vietnamese" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php //wp_body_open(); ?>
<div id="page" class="site">
    <header id="header-main">
        <?php get_template_part( 'template-parts/header/menu', 'main' ); ?>
        
    </header>

    <div id="content" class="site-content">
